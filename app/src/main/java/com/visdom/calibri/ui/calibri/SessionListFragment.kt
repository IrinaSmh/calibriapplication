package com.visdom.calibri.ui.calibri

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.neurosdk.callibri.example.utils.CommonHelper
import com.visdom.calibri.R
import com.visdom.calibri.databinding.SessionListFragmentBinding
import com.visdom.calibri.datasource.EcgDataJson
import com.visdom.calibri.datasource.SessionDto
import com.visdom.calibri.viewmodel.EcgDataViewModel
import com.visdom.calibri.viewmodel.SessionViewModel
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.direct
import org.kodein.di.generic.instance

class SessionListFragment : Fragment(), KodeinAware {
    private val viewModel: SessionViewModel by viewModels { SessionViewModel.Factory(direct.instance()) }
    private val ecgViewModel : EcgDataViewModel by activityViewModels()
    override lateinit var  kodein: Kodein
    private lateinit var binding : SessionListFragmentBinding
    private lateinit var adapter: SessionListAdapter
    private var _callback: SessionFragmentCallback? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        kodein = Kodein {
            extend((requireActivity().application as KodeinAware).kodein)
        }
        binding = SessionListFragmentBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewList: RecyclerView = view.findViewById(R.id.list)
        viewModel.getDoctorsSessions(5, false)

        binding.noFoundSessionsCard.visibility = View.INVISIBLE
        if (viewModel.sessionList.value == null) {
            binding.noFoundSessionsCard.visibility = View.VISIBLE
        }

        Log.d("S", "new session list")
        val listObserver = Observer<List<SessionDto>> { newList ->
            // Update the UI
            adapter = SessionListAdapter(newList, object : OnClickListListener{
                override fun onClicked(session: SessionDto) {
                    viewModel.clickedSession.value = session
                    val artObserver = Observer<SessionDto> {
                        if(ecgViewModel.ecg.value != null){
                            viewModel.saveSessionEcgData(EcgDataJson(
                                listOf(ecgViewModel.ecg.value!!),
                                viewModel.clickedSession.value!!.sessionId.toInt()))
                            Log.d("ECGLIST", ecgViewModel.ecg.value?.hr.toString())
                            Log.d("ECGLIST", ecgViewModel.ecg.value?.si.toString())
                            Log.d("CLICK", viewModel.clickedSession.value!!.sessionId.toString())
                            observeErrorEvent()
                            observeSuccessfulSaving()
                        }

                       // findNavController().navigate(R.id.action_articleListFragment_to_fullArticleFragment)
                    }
                    viewModel.clickedSession.observe(viewLifecycleOwner, artObserver)

//                    ecgViewModel.ecg.value?.let { it1 ->
//                        viewModel.saveSessionEcgData(it1, viewModel.clickedSession.value!!.sessionId.toInt())
//                        Log.d("ECGLIST", ecgViewModel.ecg.value?.hr.toString())
//                        Log.d("ECGLIST", ecgViewModel.ecg.value?.si.toString())
//                        Log.d("CLICK", viewModel.clickedSession.value!!.sessionId.toString())
//                    }

//                    observeErrorEvent()
//                    observeSuccessfulSaving()
                }
            })
            viewList.adapter = adapter
        }
        viewModel.sessionList.observe(viewLifecycleOwner, listObserver)

    }

    private fun observeErrorEvent() {
        viewModel.errorEvent.observe(viewLifecycleOwner, {event ->
            event.getContentIfNotHandled()?.let {
                CommonHelper.showMessage(this@SessionListFragment, "Ошибка")
            }
        })
    }

    private fun observeSuccessfulSaving(){
        viewModel.successSaving.observe(viewLifecycleOwner,  {event ->
            event.getContentIfNotHandled()?.let {
                CommonHelper.showMessage(this@SessionListFragment, "Данные успешно сохранены")
                viewModel.clickedSession.value?.sessionId?.let { it1 -> viewModel.closeSession(it1.toInt()) }
                observeSuccessfulClose()
            }

        })
    }

    private fun observeSuccessfulClose(){
        viewModel.successClose.observe(viewLifecycleOwner, {
            event ->
            event.getContentIfNotHandled()?.let {
                val cb = _callback
                cb?.account()
            }
        })
    }

    interface SessionFragmentCallback {
        fun account()
    }

    fun setCallback(callback: SessionFragmentCallback?) {
        _callback = callback
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment
         *
         * @return A new instance of SessionListFragment.
         */
        @JvmStatic
        fun newInstance(): SessionListFragment {
            return SessionListFragment()
        }
    }


}