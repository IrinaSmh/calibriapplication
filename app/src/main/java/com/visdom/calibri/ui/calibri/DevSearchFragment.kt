package com.visdom.calibri.ui.calibri

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.AdapterView.OnItemClickListener
import androidx.fragment.app.Fragment

import com.neurosdk.callibri.example.utils.CommonHelper
import com.neurosdk.callibri.example.utils.DeviceHelper.IDeviceEvent
import com.neurosdk.callibri.example.utils.SensorHelper.ISensorEvent
import com.visdom.calibri.R
import com.visdom.calibri.ui.calibri.DevHolder.Companion.inst
import java.util.*
import java.util.concurrent.Executors

/**
 * A simple [Fragment] subclass.
 * Use the [DevSearchFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class DevSearchFragment : Fragment(), ICallibriFragment {
    private val DEV_NAME_KEY = "name"
    private val DEV_ADDRESS_KEY = "address"
    private var btnSearch: Button? = null
    private var lvDevices: ListView? = null
    private var _lvDevicesAdapter: BaseAdapter? = null
    private val _deviceInfoList = ArrayList<HashMap<String, String?>>()
    private val _es = Executors.newFixedThreadPool(1)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_dev_search, container, false)
        btnSearch = rootView.findViewById(R.id.btn_search)
        btnSearch?.setOnClickListener(View.OnClickListener {
            if (inst()!!.isSearchStarted) {
                stopSearch()
            } else {
                startSearch()
            }
        })
        initDevicesListView(rootView)
        inst()!!.setDeviceEvent(object : IDeviceEvent {
            override fun searchStateChanged(searchState: Boolean) {
                btnSearch?.post(Runnable { btnSearch?.setText(if (searchState) R.string.btn_stop_search_title else R.string.btn_start_search_title) })
                unlockView()
            }

            override fun deviceListChanged() {
                updateDevicesListView()
            }
        })
        return rootView
    }

    private fun initDevicesListView(rootView: View) {
        lvDevices = rootView.findViewById(R.id.lv_devices)
        _lvDevicesAdapter = SimpleAdapter(context,
            _deviceInfoList,
            android.R.layout.simple_list_item_2, arrayOf(DEV_NAME_KEY, DEV_ADDRESS_KEY), intArrayOf(android.R.id.text1, android.R.id.text2))
        lvDevices?.setAdapter(_lvDevicesAdapter)
        lvDevices?.setOnItemClickListener(OnItemClickListener { adapterView, view, position, id ->
            val item = _lvDevicesAdapter?.getItem(position) as Map<String, String>
            if (item != null) {
                stopSearch()
                connectToDevice(item[DEV_ADDRESS_KEY])
            }
        })
    }

    private fun updateDevicesListView() {
        _deviceInfoList.clear()
        for (it in inst()!!.deviceInfoList) {
            val map = HashMap<String, String?>()
            map[DEV_NAME_KEY] = it.name()
            map[DEV_ADDRESS_KEY] = it.address()
            _deviceInfoList.add(map)
        }
        _lvDevicesAdapter!!.notifyDataSetInvalidated()
    }

    private fun clearDevicesListView() {
        if (!_deviceInfoList.isEmpty()) {
            _deviceInfoList.clear()
            _lvDevicesAdapter!!.notifyDataSetInvalidated()
        }
    }

    private fun stopSearch() {
        inst()!!.stopSearch()
    }

    private fun startSearch() {
        lockView()
        inst()!!.disconnect()
        clearDevicesListView()
        inst()!!.enabledSensor(object : ISensorEvent {
            override fun ready() {
                inst()!!.startSearch()
            }

            override fun cancel(message: String, error: Exception) {
                unlockView()
                CommonHelper.showMessage(this@DevSearchFragment, message)
            }
        })
    }

    private fun invokeDeviceConnected() {
        CommonHelper.showMessage(this@DevSearchFragment, R.string.device_search_connected)
        unlockView()
    }

    private fun lockView() {
        btnSearch!!.isEnabled = false
        lvDevices!!.isEnabled = false
    }

    private fun unlockView() {
        btnSearch!!.post {
            btnSearch!!.isEnabled = true
            lvDevices!!.isEnabled = true
        }
    }

    private fun connectToDevice(address: String?) {
        lockView()
        _es.execute {
            try {
                inst()!!.connect(address)
                invokeDeviceConnected()
            } catch (ex: Exception) {
                Log.d(TAG, "Failed connect to device", ex)
                CommonHelper.showMessage(this@DevSearchFragment, R.string.device_search_connection_failed)
                unlockView()
            }
        }
    }

    override fun stopProcess() {
        stopSearch()
    }

    companion object {
        private const val TAG = "[DevSearch]"

        /**
         * Use this factory method to create a new instance of
         * this fragment
         *
         * @return A new instance of fragment DevSearchFragment.
         */
        @JvmStatic
        fun newInstance(): DevSearchFragment {
            return DevSearchFragment()
        }
    }
}