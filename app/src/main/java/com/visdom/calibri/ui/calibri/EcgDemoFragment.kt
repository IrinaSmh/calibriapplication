package com.visdom.calibri.ui.calibri

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.androidplot.xy.XYPlot
import com.neuromd.neurosdk.*
import com.neuromd.neurosdk.ParameterName
import com.neurosdk.callibri.example.utils.CommonHelper
import com.neurosdk.callibri.example.utils.PlotHolder
import com.visdom.calibri.R
import com.visdom.calibri.datasource.EcgDataDto
import com.visdom.calibri.ui.calibri.DevHolder.Companion.inst
import com.visdom.calibri.viewmodel.AccountViewModel
import com.visdom.calibri.viewmodel.EcgDataViewModel
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.direct
import org.kodein.di.generic.instance
import java.util.ArrayList
import java.util.concurrent.Executors
import java.util.concurrent.Future
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicInteger
import java.util.concurrent.atomic.AtomicReference
import kotlin.Any
import kotlin.Boolean
import kotlin.Byte

/**
 * A simple [Fragment] subclass.
 * Use the [EcgDemoFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class EcgDemoFragment() : Fragment(), ICallibriFragment {
    private val TAG = "[SignalDemo]"
    private var plot: PlotHolder? = null
    private val _hr = AtomicInteger()
    private val _si = AtomicReference(0.0)
    private val _elState = AtomicBoolean()
    private var _callback: EcgCallback? = null
    private var _elStChannel: ElectrodesStateChannel? = null
    private var _hrChannel: HeartRateChannel? = null
    private var _siChannel: StressIndexChannel? = null
    private var _futureUpd: Future<*>? = null
    private val hrList : MutableList<Int> = ArrayList()
    private val siList : MutableList<Int> = ArrayList()
    private val viewModel: EcgDataViewModel by activityViewModels()
    //private val _sessionListFragment: SessionListFragment? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return initPlot(inflater.inflate(R.layout.fragment_ecg_demo, container, false))
    }

    private fun initPlot(rootView: View): View {
        plot = PlotHolder(rootView.findViewById<View>(R.id.plot_signal) as XYPlot)
        rootView.findViewById<View>(R.id.btn_zoom_in).setOnClickListener { plot!!.zoomYIn() }
        rootView.findViewById<View>(R.id.btn_zoom_out).setOnClickListener { plot!!.zoomYOut() }
        return rootView
    }

    private fun updateData() {
        val activity = activity
        if (activity != null && isAdded) {
            activity.runOnUiThread(Runnable {
                val view = view
                if (view != null && isAdded) {
                    val txtHRValue = view.findViewById<TextView>(R.id.txt_hr_value)
                    val txtSIValue = view.findViewById<TextView>(R.id.txt_si_value)
                    if (txtHRValue != null) {
                        val strHR = _hr.get().toString()
                        txtHRValue.text = strHR
                        hrList.add(strHR.toInt())
                        //Log.d("HR", _hr.get().toString())
                    }
                    if (txtSIValue != null) {
                        val si = Math.round(_si.get())
                        txtSIValue.text =
                            if (si > 0) si.toString() else getString(R.string.stress_index_zero)
                        siList.add(si.toInt())
                        //Log.d("SI", _si.get().toString())
                    }
                }
            })
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        try {
            val device = inst()!!.device()
            if (device != null) {
                val channelInfo = inst()!!.getDevChannel(ChannelType.Signal)
                if (channelInfo != null) {
                    configureDevice(device)
                    initChannels(device, channelInfo)
                    _futureUpd = Executors.newFixedThreadPool(1).submit {
                        try {
                            while (!Thread.currentThread().isInterrupted) {
                                Thread.sleep(500)
                                updateData()
                            }
                        } catch (ignored: Exception) {
                        }
                    }
                }
            }
        } catch (ex: Exception) {
            Log.d(TAG, "Failed start signal", ex)
            CommonHelper.showMessage(this, R.string.err_start_signal)
        }
        val btnSend = view.findViewById<Button>(R.id.btn_send)
        btnSend.setOnClickListener { l: View? ->
            viewModel.setEcgData(EcgDataDto(hrList, siList))
            viewModel.setEcgData(EcgDataDto(hrList, siList))
            Log.d("ECG", viewModel.ecg.value?.hr.toString())
            Log.d("ECG", viewModel.ecg.value?.si.toString())
            val cb = _callback
            cb!!.sessionList()
        }
    }

    private fun initChannels(device: Device, channelInfo: ChannelInfo) {
        // Create all channel
        val ecgChannel = EcgChannel(device, channelInfo)
        plot!!.startRender(ecgChannel, PlotHolder.ZoomVal.V_0002, 5.0f)
        _elStChannel = ElectrodesStateChannel(device)
        val signalChannel = SignalChannel(device, channelInfo)
        val rPeakChannel = RPeakChannel(signalChannel, _elStChannel)
        _hrChannel = HeartRateChannel(rPeakChannel)
        _siChannel =
            StressIndexChannel(rPeakChannel) // Determining the stress index can take several minutes
        _hrChannel!!.dataLengthChanged.subscribe { o, integer ->
            val ttLen = _hrChannel!!.totalLength()
            if (ttLen > 0) {
                val hrVals = _hrChannel!!.readData(ttLen - 1, 1)
                if (hrVals != null && hrVals.size > 0) _hr.lazySet(hrVals[0])
            }
        }
        _siChannel!!.dataLengthChanged.subscribe { o, integer ->
            val ttLen = _siChannel!!.totalLength()
            if (ttLen > 0) {
                val siVals = _siChannel!!.readData(ttLen - 1, 1)
                if (siVals != null && siVals.size > 0) _si.lazySet(siVals[0])
            }
        }
        _elStChannel!!.dataLengthChanged.subscribe { o, integer ->
            val ttLen = _elStChannel!!.totalLength()
            if (ttLen > 0) {
                val st = _elStChannel!!.readData(ttLen - 1, 1)
                if (st != null && st.size > 0) _elState.lazySet(st[0] == ElectrodeState.Normal)
            }
        }
    }

    private fun configureDevice(device: Device) {
        val params = device.parameters()
        for (it in params) {
            when (it.name) {
                ParameterName.Gain -> if (device.readParam<Any>(ParameterName.Gain) !== Gain.Gain6) device.setParam(
                    ParameterName.Gain, Gain.Gain6
                )
                ParameterName.Offset -> if ((device.readParam<Any>(ParameterName.Offset) as Byte).toInt () != 3
                    )device.setParam(
                    ParameterName.Offset, 3.toByte()
                )
                ParameterName.ADCInputState -> if (device.readParam<Any>(ParameterName.ADCInputState) !== ADCInput.Resistance) device.setParam(
                    ParameterName.ADCInputState, ADCInput.Resistance
                )
                ParameterName.SamplingFrequency -> if (device.readParam<Any>(ParameterName.SamplingFrequency) !== SamplingFrequency.Hz125) device.setParam(
                    ParameterName.SamplingFrequency, SamplingFrequency.Hz125
                )
                ParameterName.HardwareFilterState -> if (device.readParam<Any>(ParameterName.HardwareFilterState) as Boolean) device.setParam(
                    ParameterName.HardwareFilterState, true
                )
                ParameterName.ExternalSwitchState -> if (device.readParam<Any>(ParameterName.ExternalSwitchState) !== ExternalSwitchInput.MioElectrodes) device.setParam(
                    ParameterName.ExternalSwitchState, ExternalSwitchInput.MioElectrodes
                )
            }
        }
        device.execute(Command.StartSignal)
    }

    override fun stopProcess() {
        if (plot != null) {
            plot!!.stopRender()
            try {
                if (_elStChannel != null) {
                    _elStChannel!!.dataLengthChanged.unsubscribe()
                    _elStChannel = null
                }
                if (_hrChannel != null) {
                    _hrChannel!!.dataLengthChanged.unsubscribe()
                    _hrChannel = null
                }
                if (_siChannel != null) {
                    _siChannel!!.dataLengthChanged.unsubscribe()
                    _siChannel = null
                }
                val futureUpd = _futureUpd
                if (futureUpd != null) {
                    _futureUpd = null
                    futureUpd.cancel(true)
                }
                val device = inst()!!.device()
                if (device != null) {
                    if (device.readParam<Any>(ParameterName.State) === DeviceState.Connected) {
                        device.execute(Command.StopSignal)
                    }
                }
            } catch (ex: Exception) {
                Log.d(TAG, "Failed stop signal", ex)
                CommonHelper.showMessage(this, R.string.err_stop_signal)
            }
        }
    }

    interface EcgCallback {
        fun sessionList()
    }

    fun setCallback(callback: EcgCallback?) {
        _callback = callback
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment
         *
         * @return A new instance of fragment DemoModeFragment.
         */
        fun newInstance(): EcgDemoFragment {
            return EcgDemoFragment()
        }
    }
}

