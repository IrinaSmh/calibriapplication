package com.visdom.calibri.ui.calibri

import android.text.TextUtils
import android.util.Pair
import androidx.appcompat.app.AppCompatActivity
import com.neuromd.neurosdk.*
import com.neuromd.neurosdk.ParameterName
import com.neurosdk.callibri.example.utils.DeviceHelper
import com.neurosdk.callibri.example.utils.DeviceHelper.IDeviceEvent
import com.neurosdk.callibri.example.utils.SensorHelper
import com.neurosdk.callibri.example.utils.SensorHelper.ISensorEvent
import com.visdom.calibri.MainActivity
import java.lang.ref.WeakReference
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.locks.ReentrantLock

internal class DevHolder {
    private var _sensorHelper: SensorHelper? = null
    private var _deviceHelper: DeviceHelper? = null
    private var _wCtx: WeakReference<AppCompatActivity>? = null
    private val _deviceLock = ReentrantLock()
    private var _device: Pair<String?, Device?>? = null
    private var _batteryChannel: BatteryChannel? = null
    private val _callbacks = Collections.synchronizedList(ArrayList<IDeviceHolderCallback>())
    private val _connectionState = AtomicBoolean()
    fun init(context: MainActivity) {
        if (context != null) {
            _wCtx = WeakReference(context)
            _sensorHelper = SensorHelper(context)
            _deviceHelper = DeviceHelper(context)
        }
    }

    fun setDeviceEvent(devEvent: IDeviceEvent?) {
        _deviceHelper!!.setDeviceEvent(devEvent)
    }

    val deviceInfoList: List<DeviceInfo>
        get() = _deviceHelper!!.deviceInfoList
    val isSearchStarted: Boolean
        get() = _deviceHelper!!.isSearchStarted

    fun stopSearch() {
        _deviceHelper!!.stopSearch()
    }

    fun startSearch() {
        _deviceHelper!!.startSearch()
    }

    fun enabledSensor(callback: ISensorEvent?) {
        _sensorHelper!!.enabledSensor(callback)
    }

    private fun invokeBatteryChanged(`val`: Int) {
        val ctx = _wCtx!!.get()
        ctx?.runOnUiThread {
            for (cb in _callbacks) {
                cb.batteryChanged(`val`)
            }
        }
    }

    private fun invokeDevStateChanged(`val`: Boolean) {
        val ctx = _wCtx!!.get()
        if (ctx != null && _connectionState.getAndSet(`val`) != `val`) {
            ctx.runOnUiThread(Runnable {
                for (cb in _callbacks) {
                    cb.deviceState(`val`)
                }
            })
        }
    }

    private fun initBatteryChannel(device: Device?) {
        if (_batteryChannel == null) {
            _batteryChannel = BatteryChannel(device)
            _batteryChannel!!.dataLengthChanged.subscribe { o, integer ->
                val bCh = _batteryChannel
                if (bCh != null) {
                    val batVal = bCh.readData(bCh.totalLength() - 1, 1)
                    if (batVal != null && batVal.size > 0) invokeBatteryChanged(batVal[0])
                }
            }
        }
    }

    private fun initDevState(device: Device?) {
        if (device != null) {
            device.parameterChanged.unsubscribe()
            device.parameterChanged.subscribe { o, parameterName ->
                if (parameterName == ParameterName.State) {
                    val dev = device()
                    if (dev != null) {
                        invokeDevStateChanged(dev.readParam<Any>(ParameterName.State) === DeviceState.Connected)
                    }
                }
            }
        }
    }

    private fun freeBatteryChannel() {
        if (_batteryChannel != null) {
            _batteryChannel!!.dataLengthChanged.unsubscribe()
            _batteryChannel = null
        }
    }

    @Throws(InterruptedException::class)
    private fun freeDevice() {
        if (_device != null) {
            _device!!.second!!.parameterChanged.unsubscribe()
            _device!!.second!!.disconnect()
            _device!!.second!!.close()
            _device = null
        }
        freeBatteryChannel()
    }

    @Throws(Exception::class)
    private fun connect(device: Device?): Device? {
        if (device!!.readParam<Any>(ParameterName.State) !== DeviceState.Connected) {
            try {
                initDevState(device)
                device!!.connect()
            } catch (ex: Exception) {
                device!!.parameterChanged.unsubscribe()
                device.close()
                throw ex
            }
            initBatteryChannel(device)
        } else {
            device!!.execute(Command.FindMe)
        }
        return device
    }

    @Throws(Exception::class)
    fun connect(address: String?) {
        if (!TextUtils.isEmpty(address)) {
            if (_deviceLock.tryLock(100, TimeUnit.MILLISECONDS)) {
                try {
                    if (_device != null && !TextUtils.equals(_device!!.first, address)) {
                        freeDevice()
                    }
                    if (_device == null) {
                        _device = Pair(address, connect(_deviceHelper!!.createDevice(address)))
                    } else {
                        connect(_device!!.second)
                    }
                } finally {
                    _deviceLock.unlock()
                }
            }
        }
    }

    fun disconnect() {
        try {
            if (_deviceLock.tryLock(100, TimeUnit.MILLISECONDS)) {
                try {
                    freeDevice()
                } finally {
                    _deviceLock.unlock()
                    invokeDevStateChanged(false)
                }
            }
        } catch (ex: Exception) {
            // skip
        }
    }

    fun device(): Device? {
        try {
            if (_deviceLock.tryLock(100, TimeUnit.MILLISECONDS)) {
                return try {
                    if (_device != null) _device!!.second else null
                } finally {
                    _deviceLock.unlock()
                }
            }
        } catch (e: InterruptedException) {
            // skip
        }
        return null
    }

    fun addCallback(callback: IDeviceHolderCallback?) {
        if (callback == null) return
        if (!_callbacks.contains(callback)) _callbacks.add(callback)
    }

    fun removeCallback(callback: IDeviceHolderCallback?) {
        if (callback == null) return
        _callbacks.remove(callback)
    }

    fun getDevChannel(channelType: ChannelType): ChannelInfo? {
        try {
            if (_deviceLock.tryLock(100, TimeUnit.MILLISECONDS)) {
                try {
                    if (_device != null && _device!!.second != null) {
                        val channelInfos = _device!!.second!!.channels()
                        if (channelInfos != null) {
                            for (it in channelInfos) {
                                if (it.type == channelType) return it
                            }
                        }
                    }
                } finally {
                    _deviceLock.unlock()
                }
            }
        } catch (e: InterruptedException) {
            // skip
        }
        return null
    }

    fun hasParam(parameterName: ParameterName): Boolean {
        try {
            if (_deviceLock.tryLock(100, TimeUnit.MILLISECONDS)) {
                try {
                    if (_device != null && _device!!.second != null) {
                        val parameters = _device!!.second!!.parameters()
                        if (parameters != null) {
                            for (it in parameters) {
                                if (it.name == parameterName) return true
                            }
                        }
                    }
                } finally {
                    _deviceLock.unlock()
                }
            }
        } catch (e: InterruptedException) {
            // skip
        }
        return false
    }

    interface IDeviceHolderCallback {
        fun batteryChanged(`val`: Int)
        fun deviceState(state: Boolean)
    }

    companion object {
        private const val TAG = "[DevHolder]"
        private val _mutex = Any()

        @Volatile
        private var _inst: DevHolder? = null
        @JvmStatic
        fun inst(): DevHolder? {
            var instRef = _inst
            if (instRef == null) {
                synchronized(_mutex) {
                    instRef = _inst
                    if (instRef == null) {
                        instRef = DevHolder()
                        _inst = instRef
                    }
                }
            }
            return instRef
        }
    }
}