package com.visdom.calibri.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.google.android.material.snackbar.Snackbar
import com.neuromd.neurosdk.ChannelType
import com.neuromd.neurosdk.DeviceState
import com.neuromd.neurosdk.ParameterName
import com.visdom.calibri.R
import com.visdom.calibri.databinding.AccountFragmentBinding
import com.visdom.calibri.ui.calibri.DevHolder
import com.visdom.calibri.viewmodel.AccountViewModel
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.direct
import org.kodein.di.generic.instance

class AccountFragment() : Fragment(), KodeinAware {
    private val viewModel: AccountViewModel by viewModels { AccountViewModel.Factory(direct.instance()) }
    override lateinit var  kodein: Kodein
    private lateinit var binding : AccountFragmentBinding
    private var _callback: AccountCallback? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        kodein = Kodein {
            extend((requireActivity().application as KodeinAware).kodein)
        }
        binding = AccountFragmentBinding.inflate(inflater, container, false)

        // Device search mode click
        binding.btnSearch.setOnClickListener {
                val cb = _callback
                cb?.modeDevSearch()
        }

        // Ecg mode click
        binding.btnEcgDemo.setOnClickListener {
            val cb = _callback
            cb?.modeEcgDemo()
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeUser()
        observeErrorEvent()
        viewModel.fetchAccountInfo()
        updateButtonState()
    }



    @SuppressLint("SetTextI18n")
    private fun observeUser() {
        viewModel.user.observe(viewLifecycleOwner, {
            binding.userNameTextView.text = "${it.name} ${it.surname}"
        })
    }

    private fun observeErrorEvent() {
        viewModel.errorEvent.observe(viewLifecycleOwner, { event ->
            event.getContentIfNotHandled()?.let {
                Snackbar.make(requireView(), it, Snackbar.LENGTH_SHORT).show()
            }
        })
    }


    fun setCallback(callback: AccountCallback?) {
        _callback = callback
    }

    fun updateButtonState() {
        val view = view ?: return
        val device = DevHolder.inst()?.device()

        if (device == null || device.readParam<Any>(ParameterName.State) == DeviceState.Disconnected) {
            view.findViewById<View>(R.id.btn_ecg_demo).isEnabled = false
        } else {
            val chInfSig = DevHolder.inst()?.getDevChannel(ChannelType.Signal)
            view.findViewById<View>(R.id.btn_ecg_demo).isEnabled = chInfSig != null
           // view.findViewById<View>(R.id.btn_mems_demo).isEnabled = DevHolder.inst()?.getDevChannel(ChannelType.MEMS) != null
           // view.findViewById<View>(R.id.btn_stimulator_demo).isEnabled = DevHolder.inst()?.hasParam(ParameterName.StimulatorParamPack) == true
        }
    }

    interface AccountCallback {
        fun modeDevSearch()
        fun modeEcgDemo()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment
         *
         * @return A new instance of fragment DemoModeFragment.
         */
        @JvmStatic
        fun newInstance(): AccountFragment {
            return AccountFragment()
        }
    }
}

