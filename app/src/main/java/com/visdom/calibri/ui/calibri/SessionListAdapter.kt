package com.visdom.calibri.ui.calibri

import android.annotation.SuppressLint
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.visdom.calibri.R
import com.visdom.calibri.datasource.SessionDto
import java.time.LocalDateTime

class SessionListAdapter(
    private val sessions : List<SessionDto>,
    private val onClickListener : OnClickListListener
) : RecyclerView.Adapter<SessionListAdapter.ViewHolder>() {
    private fun getItem(position: Int) = sessions[position]

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: ViewHolder, position: Int){
        val session : SessionDto = getItem(position)
        holder.bind(session)
        holder.itemView.setOnClickListener {
            onClickListener.onClicked(session)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun getItemCount() = sessions.size


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val id : TextView = itemView.findViewById(R.id.item_id)
        private val date : TextView = itemView.findViewById(R.id.item_date)
        private val fio : TextView = itemView.findViewById(R.id.item_FIO)

        @RequiresApi(Build.VERSION_CODES.O)
        @SuppressLint("SetTextI18n")
        fun bind(session: SessionDto){
            val parsedDate = LocalDateTime.parse(session.date)
            id.text = "ID сессии: " + session.sessionId
            val month : String
            month = if(parsedDate.monthValue < 10){
                "0${parsedDate.monthValue}"
            } else parsedDate.monthValue.toString()
            date.text = "Дата: " + parsedDate.year.toString() + "-" + month + "-" + parsedDate.dayOfMonth + " Время: " +
                    parsedDate.hour + ":" + parsedDate.minute + ":" + parsedDate.second
            fio.text = session.patient.surname + " " + session.patient.name + " " + session.patient.patronymic
        }

        companion object {
            fun from(parent: ViewGroup) : ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                return ViewHolder(layoutInflater.inflate(R.layout.row_item, parent, false))
            }
        }
    }
}

interface OnClickListListener {
    fun onClicked(session : SessionDto)
}