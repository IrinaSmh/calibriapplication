package com.visdom.calibri.ui

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.visdom.calibri.viewmodel.LoginViewModel
import com.neurosdk.callibri.example.utils.CommonHelper
import com.visdom.calibri.databinding.LoginFragmentBinding
import com.visdom.calibri.domain.InternetCheck
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.direct
import org.kodein.di.generic.instance

class LoginFragment() : Fragment(), KodeinAware {

    private val viewModel: LoginViewModel by viewModels { LoginViewModel.Factory(direct.instance()) }
    private lateinit var binding: LoginFragmentBinding
    override lateinit var  kodein: Kodein
    private var _callback: LoginCallback? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        kodein = Kodein {
            extend((requireActivity().application as KodeinAware).kodein)
        }
        binding = LoginFragmentBinding.inflate(inflater, container, false)

        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupLoginButton()
        observeErrorEvent()
        observeAuthorizedEvent()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun setupLoginButton() {
        binding.loginButton.setOnClickListener {
            if(context?.let { it1 -> InternetCheck.isOnline(it1) } == true){
                viewModel.login(
                    binding.loginEditText.text.toString(),
                    binding.passwordEditText.text.toString()
                )
            }
           else CommonHelper.showMessage(this@LoginFragment, "Ошибка подключения. Проверьте доступ в Интернет.")
        }
    }

    private fun observeErrorEvent() {
        viewModel.errorEvent.observe(viewLifecycleOwner, {event ->
            event.getContentIfNotHandled()?.let {
                CommonHelper.showMessage(this@LoginFragment, "Ошибка авторизации")
            }
        })
    }

    private fun observeAuthorizedEvent() {
        viewModel.authorizedGameEvent.observe(viewLifecycleOwner, { event ->
            event?.getContentIfNotHandled()?.let {
//                val fm = parentFragmentManager
//                val ft = fm.beginTransaction()
//                if (_accountFragment == null) {
//                    _accountFragment = AccountFragment.newInstance()
//                }
//                ft.replace(R.id.container, _accountFragment!!)
//                ft.commit()
               // CommonHelper.showMessage(this@LoginFragment, "Вы вошли как доктор.")
                val cb = _callback
                cb?.account()
            }
        })
    }

    fun setCallback(callback: LoginCallback?) {
        _callback = callback
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment
         *
         * @return A new instance of fragment DemoModeFragment.
         */
        @JvmStatic
        fun newInstance(): LoginFragment {
            return LoginFragment()
        }
    }

    interface LoginCallback {
        fun account()
    }
}