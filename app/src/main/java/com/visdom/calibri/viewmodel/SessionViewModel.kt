package com.visdom.calibri.viewmodel

import android.util.Log
import androidx.lifecycle.*
import com.visdom.calibri.R
import com.visdom.calibri.datasource.EcgDataJson
import com.visdom.calibri.datasource.SessionDto
import com.visdom.calibri.domain.Result
import com.visdom.calibri.repository.SessionRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class SessionViewModel (private val sessionRepository: SessionRepository) : ViewModel(){
    private val _sessionsList = MutableLiveData<List<SessionDto>>()
    val sessionList: LiveData<List<SessionDto>> = _sessionsList

    private val _errorEvent = MutableLiveData<Event<Int>>()
    val errorEvent: LiveData<Event<Int>> = _errorEvent

    private val _successSaving = MutableLiveData<Event<Unit>>()
    val successSaving: LiveData<Event<Unit>> = _successSaving

    private val _successClose = MutableLiveData<Event<Unit>>()
    val successClose: LiveData<Event<Unit>> = _successClose

    val clickedSession: MutableLiveData<SessionDto> by lazy {
        MutableLiveData<SessionDto>()
    }

    fun saveSessionEcgData(ecgDataDto: EcgDataJson){
        viewModelScope.launch {
            val result = sessionRepository.saveEcgData(ecgDataDto)
            when(result) {
                is Result.Success -> _successSaving.value = Event(Unit)
                is Result.Error -> _errorEvent.value = Event(R.string.error_with_ecg_data_save)
            }
        }
    }

    fun closeSession(sessionId : Int){
        viewModelScope.launch {
            val result = sessionRepository.closeSession(sessionId)
            when(result) {
                is Result.Success -> _successClose.value = Event(Unit)
                is Result.Error -> _errorEvent.value = Event(R.string.error_with_session_close)
            }
        }
    }

    fun getDoctorsSessions(hours : Int, isFinished : Boolean){
        viewModelScope.launch(Dispatchers.IO) {
            Log.d("S", "start get session")
            when (val result = sessionRepository.getSessions(hours, isFinished)) {
                is Result.Success -> {
                    _sessionsList.postValue(result.data)
                    Log.d("S", "good get session")
                }
                is Result.Error -> _errorEvent.postValue(Event(R.string.error_with_session_get))
            }

        }
    }

    class Factory(private val sessionRepository: SessionRepository) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(SessionViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return SessionViewModel(sessionRepository) as T
            }
            throw IllegalArgumentException("Unable to construct viewmodel")
        }
    }
}