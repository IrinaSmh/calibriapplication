package com.visdom.calibri.viewmodel

import androidx.lifecycle.*
import com.visdom.calibri.datasource.EcgDataDto
import kotlinx.coroutines.launch

class EcgDataViewModel() : ViewModel() {
    private val _ecg = MutableLiveData<EcgDataDto>()
    val ecg: LiveData<EcgDataDto> = _ecg

    fun setEcgData(ecgDataDto: EcgDataDto){
        viewModelScope.launch {
            _ecg.value = ecgDataDto
        }
    }
}