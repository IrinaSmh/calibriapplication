package com.visdom.calibri

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.visdom.calibri.ui.AccountFragment
import com.visdom.calibri.ui.LoginFragment
import com.visdom.calibri.ui.calibri.*
import com.visdom.calibri.ui.calibri.DevHolder
import com.visdom.calibri.ui.calibri.DevHolder.Companion.inst

class MainActivity : AppCompatActivity() {
    private var _loginFragment: LoginFragment? = null
    private var _accountFragment: AccountFragment? = null
    private var _deDevSearchFragment: DevSearchFragment? = null
    private var _ecgDemoFragment: EcgDemoFragment? = null
    private var _sessionListFragment : SessionListFragment? = null
    private var txtDevState: TextView? = null
    private var txtDevBatteryPower: TextView? = null
    private var llDeviceState : LinearLayout? = null
    private var _demoMode: DemoMode? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        llDeviceState = findViewById(R.id.ll_device_state)

        txtDevState = findViewById(R.id.txt_dev_state)
        txtDevBatteryPower = findViewById(R.id.txt_dev_battery_power)

        deviceInfoSettings()
        showLoginFragment()
    }

    private fun deviceInfoSettings(){
        inst()!!.init(this)
        inst()!!.addCallback(object : DevHolder.IDeviceHolderCallback {
            override fun batteryChanged(`val`: Int) {
                txtDevBatteryPower?.text = getString(R.string.dev_power_prc, `val`)
            }

            override fun deviceState(state: Boolean) {
                if (state) {
                    txtDevState?.setText(R.string.dev_state_connected)
                } else {
                    txtDevState?.setText(R.string.dev_state_disconnected)
                    txtDevBatteryPower?.setText(R.string.dev_power_empty)
                }
                updateContentFragment(state)
            }
        })
    }

    override fun onBackPressed() {
        if (_demoMode == DemoMode.START) super.onBackPressed()
       // else if(_demoMode == DemoMode.SESSIONS){
            //showEcgDemoMode()
        //}
         else showAccount()
    }



    private fun updateContentFragment(connectionState: Boolean) {
        updateDemoModeButton(connectionState)
        if (connectionState || _demoMode != DemoMode.DEV_SEARCH) showAccount()
    }

    private fun updateDemoModeButton(connectionState: Boolean) {
        if (!connectionState) {
            if (_accountFragment == null || _demoMode != DemoMode.START) return
            _accountFragment!!.updateButtonState()
        }
    }


    private fun showLoginFragment(){
        llDeviceState?.visibility = View.INVISIBLE
        stopProcess()
        val fm = supportFragmentManager
        val ft = fm.beginTransaction()
        if (_loginFragment == null) {
            _loginFragment = LoginFragment.newInstance()
            _loginFragment!!.setCallback(object : LoginFragment.LoginCallback{
                override fun account() {
                    showAccount()
                }
            })

        }
        ft.replace(R.id.container, _loginFragment!!)
        ft.commit()
        _demoMode = DemoMode.LOGIN
    }

    private fun showAccount() {
        llDeviceState?.visibility = View.VISIBLE
       // deviceInfoSettings()
        stopProcess()
        val fm = supportFragmentManager
        val ft = fm.beginTransaction()
        if (_accountFragment == null) {
            _accountFragment = AccountFragment.newInstance()
            _accountFragment!!.setCallback(object : AccountFragment.AccountCallback {
                override fun modeDevSearch() {
                    showDevSearch()
                }

                override fun modeEcgDemo() {
                    showEcgDemoMode()
                }

            })


        }
        ft.replace(R.id.container, _accountFragment!!)
        ft.commit()
        _demoMode = DemoMode.START
    }

    private fun showDevSearch() {
        stopProcess()
        val fm = supportFragmentManager
        val ft = fm.beginTransaction()
        if (_deDevSearchFragment == null) {
            _deDevSearchFragment = DevSearchFragment.newInstance()
        }
        ft.replace(R.id.container, _deDevSearchFragment!!)
        ft.commit()
        _demoMode = DemoMode.DEV_SEARCH

    }

    private fun showEcgDemoMode() {
        stopProcess()
        val fm = supportFragmentManager
        val ft = fm.beginTransaction()
        if (_ecgDemoFragment == null) {
            _ecgDemoFragment = EcgDemoFragment.newInstance()
            _ecgDemoFragment!!.setCallback(object : EcgDemoFragment.EcgCallback{
                override fun sessionList() {
                   showSessionList()
                }

            })
        }
        ft.replace(R.id.container, _ecgDemoFragment!!)
        ft.commit()
        _demoMode = DemoMode.ECG_MODE
    }

    private fun showSessionList(){
        llDeviceState?.visibility = View.INVISIBLE
        stopProcess()
        val fm = supportFragmentManager
        val ft = fm.beginTransaction()
        if (_sessionListFragment == null) {
            _sessionListFragment = SessionListFragment.newInstance()
            _sessionListFragment!!.setCallback(object : SessionListFragment.SessionFragmentCallback{
                override fun account() {
                    showAccount()
                }

            })
        }
        ft.replace(R.id.container, _sessionListFragment!!)
        ft.commit()
        _demoMode = DemoMode.SESSIONS
    }

    private fun stopProcess() {
        val fm = supportFragmentManager
        for (it in fm.fragments) {
            if (it.javaClass.isAssignableFrom(ICallibriFragment::class.java)) {
                (it as ICallibriFragment).stopProcess()
            }
        }
    }


    private enum class DemoMode {
        LOGIN, START, DEV_SEARCH,  ECG_MODE, SESSIONS
    }

}