package com.visdom.calibri

import android.app.Application
import com.example.calibriapp.dataKodeinModule
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.description
import org.kodein.di.generic.bind
import org.kodein.di.generic.provider
import timber.log.Timber

class Calibri : Application(), KodeinAware {

    override val kodein: Kodein = Kodein {
        import(dataKodeinModule)
        bind<Calibri>() with provider { this@Calibri }
    }

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
        Timber.d(kodein.container.tree.bindings.description())
    }
}