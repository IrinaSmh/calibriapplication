package com.visdom.calibri.datasource

import com.google.gson.annotations.SerializedName

data class SessionDto(@SerializedName("id") val sessionId: String,
                      @SerializedName("creationDateTime") val date: String,
                      @SerializedName("patient") val patient: PatientInfoDto) {
}