package com.visdom.calibri.datasource

import com.google.gson.annotations.SerializedName
import java.util.*

data class EcgDataDto(
    //@SerializedName("plotData") val plotData : List<Map<Date, Int>>,
     @SerializedName("hr") val hr : List<Int>,
     @SerializedName("si") val si : List<Int>
    )
