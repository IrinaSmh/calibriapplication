package com.visdom.calibri.datasource

import com.google.gson.annotations.SerializedName

data class EcgDataJson(@SerializedName("callibriData") val ecgData : List<EcgDataDto>,
                       @SerializedName("sessionId") val sessionId : Int)
