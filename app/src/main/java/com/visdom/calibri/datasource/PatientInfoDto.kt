package com.visdom.calibri.datasource

import com.google.gson.annotations.SerializedName

data class PatientInfoDto (@SerializedName("name") val name: String,
                           @SerializedName("surname") val surname: String,
                           @SerializedName("patronymic") val patronymic: String){
}