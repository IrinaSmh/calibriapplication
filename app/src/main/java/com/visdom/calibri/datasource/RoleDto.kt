package com.visdom.calibri.datasource

import com.google.gson.annotations.SerializedName

data class RoleDto(@SerializedName("id") val id: Long,
                   @SerializedName("name") val name: String,
                   @SerializedName("description") val description: String)