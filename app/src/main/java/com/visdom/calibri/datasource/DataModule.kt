package com.example.calibriapp

import com.visdom.calibri.domain.networkKodeinModule
import com.visdom.calibri.repository.*
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

val dataKodeinModule = Kodein.Module("DataKodeinModule"){
    import(networkKodeinModule)
    bind<CoroutineDispatcher>() with provider { Dispatchers.IO }
    bind<UserManager>() with singleton { UserManager(instance()) }
    bind<LoginRepository>() with singleton { LoginRepositoryImpl(instance(), instance(), instance()) }
    bind<UserRepository>() with singleton { UserRepositoryImpl(instance(), instance()) }
    bind<SessionRepository>() with singleton { SessionRepositoryImpl(instance(), instance()) }
}