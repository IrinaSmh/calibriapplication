package com.visdom.calibri.datasource

import com.google.gson.annotations.SerializedName

data class UserDto(@SerializedName("id") val userId: String,
                   @SerializedName("name") val name: String,
                   @SerializedName("surname") val surname: String,
                   @SerializedName("email") val email: String,
                   @SerializedName("patronymic") val patronymic: String,
                   @SerializedName("roles") val roles: List<RoleDto>)
