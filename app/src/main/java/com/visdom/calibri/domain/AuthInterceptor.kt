package com.visdom.calibri.domain

import com.visdom.calibri.repository.UserManager
import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor(private val userManager: UserManager) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {

        val builder = chain.request().newBuilder()
        if(!chain.request().url.toUrl().path.contains("registration")) {
            builder.addHeader("Authorization", userManager.token)
        }
        return chain.proceed(builder.build())
    }

}