package com.visdom.calibri.domain

import com.visdom.calibri.datasource.EcgDataDto
import com.visdom.calibri.datasource.EcgDataJson
import com.visdom.calibri.datasource.SessionDto
import retrofit2.Response
import retrofit2.http.*

interface SessionApiService {
    @GET("/devirta_backend/api/sessions/my/recent/{hours}/isFished/{isFinished}")
    suspend fun getDoctorsSessions(@Path("hours") hours: Int, @Path("isFinished") isFinished: Boolean) : Response<List<SessionDto>>

    @POST("/devirta_backend/api/session/calibri")
    suspend fun saveCalibriData(@Body ecgData : EcgDataJson) : Response<Unit>

    @PUT("/devirta_backend/api/sessions/session/close/{sessionId}")
    suspend fun closeSession(@Path("sessionId") sessionId: Int) : Response<Unit>
}