package com.visdom.calibri.domain

import com.visdom.calibri.datasource.UserDto
import retrofit2.Response
import retrofit2.http.GET

interface UserApiService {
    @GET("api/users/user/login/")
    suspend fun login() : Response<UserDto>
}