package com.visdom.calibri.repository

import com.visdom.calibri.datasource.EcgDataJson
import com.visdom.calibri.datasource.SessionDto
import com.visdom.calibri.domain.Result

interface SessionRepository {
    suspend fun getSessions(hours : Int, isFinished : Boolean) : Result<List<SessionDto>>
    suspend fun saveEcgData(ecgData : EcgDataJson) : Result<Unit>
    suspend fun closeSession(sessionId : Int) : Result<Unit>
}
