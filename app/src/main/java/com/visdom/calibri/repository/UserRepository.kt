package com.visdom.calibri.repository

import com.visdom.calibri.datasource.UserDto
import com.visdom.calibri.domain.Result

interface UserRepository {
    suspend fun getUserInfo() : Result<UserDto>
}