package com.visdom.calibri.repository

import com.visdom.calibri.datasource.EcgDataJson
import com.visdom.calibri.datasource.SessionDto
import com.visdom.calibri.domain.SessionApiService
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import com.visdom.calibri.domain.Result

class SessionRepositoryImpl(
    private val sessionApi : SessionApiService,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : SessionRepository {
    override suspend fun getSessions(hours : Int, isFinished : Boolean) : Result<List<SessionDto>> = withContext(ioDispatcher){
        return@withContext try {
            val response = sessionApi.getDoctorsSessions(hours, isFinished)
            if(response.code() == 200) Result.Success(response.body()!!) else Result.Error(Exception(""))
        } catch (e: Exception) {
            Result.Error(e)
        }
    }

    override suspend fun saveEcgData(ecgData: EcgDataJson) : Result<Unit> = withContext(ioDispatcher){
        return@withContext try {
            val response = sessionApi.saveCalibriData(ecgData)
            if(response.code() == 200) Result.Success(response.body()!!) else Result.Error(Exception("Invalid session params"))
        } catch (e: Exception) {
            Result.Error(e)
        }
    }

    override suspend fun closeSession(sessionId: Int): Result<Unit> = withContext(ioDispatcher){
        return@withContext try {
            val response = sessionApi.closeSession(sessionId)
            if(response.code() == 200) Result.Success(response.body()!!) else Result.Error(Exception("Invalid session params"))
        } catch (e: Exception) {
            Result.Error(e)
        }
    }
}