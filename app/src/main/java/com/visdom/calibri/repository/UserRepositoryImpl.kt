package com.visdom.calibri.repository

import com.visdom.calibri.datasource.UserDto
import com.visdom.calibri.domain.UserApiService
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import com.visdom.calibri.domain.Result

class UserRepositoryImpl(
    private val userEndpoints: UserApiService,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : UserRepository {
    override suspend fun getUserInfo(): Result<UserDto> = withContext(ioDispatcher) {
        return@withContext try {
            val response = userEndpoints.login()
            if(response.code() == 200) Result.Success(response.body()!!) else Result.Error(Exception(""))
        } catch (e: Exception) {
            Result.Error(e)
        }
    }

}