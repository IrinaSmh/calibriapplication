package com.visdom.calibri.repository

import com.visdom.calibri.datasource.UserDto
import com.visdom.calibri.domain.UserApiService
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.Credentials
import com.visdom.calibri.domain.Result

class LoginRepositoryImpl(
    private val userEndpoints: UserApiService,
    private val userManager: UserManager,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : LoginRepository {
    override suspend fun login(phone: String, password: String): Result<UserDto> =
        withContext(ioDispatcher) {
            userManager.token = Credentials.basic(phone, password)
            val response = userEndpoints.login()
            var isDoctor = false
            response.body()?.roles?.forEach {
                if(it.id == 2L) isDoctor = true
            }
            return@withContext try {
                if (response.code() == 200 && isDoctor) Result.Success(response.body()!!) else Result.Error(Exception("Unauthorized"))
            } catch (e: Exception) {
                Result.Error(e)
            }
        }
}