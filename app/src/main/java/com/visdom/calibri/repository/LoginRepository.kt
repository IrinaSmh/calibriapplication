package com.visdom.calibri.repository

import com.visdom.calibri.datasource.UserDto
import com.visdom.calibri.domain.Result
interface LoginRepository {
    suspend fun login(phone: String, password: String) : Result<UserDto>
}